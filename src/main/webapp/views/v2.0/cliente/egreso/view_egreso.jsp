<div class="form-title"><h4>REPORTE DE EGRESO</h4></div>

<form class="form-container">
	<TABLE>
		<TR>
			<TD>
				<div class="form-title">FECHA DE EGRESO</div>
				<input class="form-field" type="text" name="fechaEgresoCliente" id="fechaEgresoCliente" readonly/>
			</TD>
			<TD>
				<div class="form-title">HORA DE EGRESO</div>
				<input class="form-field" type="text" name="horaEgresoCliente" id="horaEgresoCliente" readonly />
			</TD>
			<TD>
				<div class="form-title">DIAGN�STICO DE EGRESO</div>
				<input class="form-field" type="text" name="icd3Cliente" id="icd3Cliente" readonly/>
			</TD>
		</TR>
		<TR>
			<TD>
				<div class="form-title">TIPO DE PROCEDIMIENTO 1</div>
				<input class="form-field" type="text" name="cpt1Cliente" id="cpt1Cliente" readonly />
			</TD>
			<TD>
				<div class="form-title">TIPO DE PROCEDIMIENTO 2</div>
				<input class="form-field" type="text" name="cpt2Cliente" id="cpt2Cliente" readonly/>
			</TD>
			<TD>
				<div class="form-title">MOTIVO DE EGRESO</div>
				<input class="form-field" type="text" name="motivoEgresoCliente" id="motivoEgresoCliente" readonly/>
			</TD>
		</TR>
		<TR>
			<TD>
				<div class="form-title">EVENTOS NO DESEABLES DEL ENTORNO HOSPITALARIO:</div>
				<input class="form-field" type="text" id ="eventosNoDeseablesCliente" name="eventosNoDeseablesCliente" readonly/>
			</TD>
			<TD>
				<div class="form-title">D�AS INCAPACIDAD</div>
				<input class="form-field" type="text" name="diasIncapacidadCliente" id="diasIncapacidadCliente" readonly/>
			</TD>
			<TD>
			</TD>
		</TR>
	</TABLE>
</form>
